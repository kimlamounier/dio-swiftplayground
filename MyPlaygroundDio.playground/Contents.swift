import Foundation

let name: String = "Steve "
var lastName: String? = "Jobs"

print(name + (lastName ?? "Wosniak"))

if let secondName = Optional(lastName!) {
    print("full name is \(name)\(secondName)")
} else{
    print("second name is nil")
}
